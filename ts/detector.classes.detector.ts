import * as plugins from './detector.plugins.js';

export class Detector {
  private smartnetworkInstance = new plugins.smartnetwork.SmartNetwork();

  public async isActive(urlArg: string): Promise<boolean> {
    const parsedUrl = plugins.smarturl.Smarturl.createFromUrl(urlArg);
    if (parsedUrl.hostname === 'localhost') {
      console.log(`detector target is localhost on port ${parsedUrl.port}`);
      const portUnused = await this.smartnetworkInstance.isLocalPortUnused(
        parseInt(parsedUrl.port, 10)
      );
      const portAvailable = !portUnused;
      return portAvailable;
    } else {
      console.log(`detector target is remote domain ${parsedUrl.host} on port ${parsedUrl.port}`);
      const postAvailable = await this.smartnetworkInstance.isRemotePortAvailable(
        parsedUrl.host,
        parseInt(parsedUrl.port, 10)
      );
      return postAvailable;
    }
  }

  public detectType(urlArg: string) {
    console.log('TODO'); // TODO
  }
}
